package robert.bleyl.persondatabase.modules.person;

import java.time.LocalDate;

public class PersonSearchRequest {

	private String firstname;
	private String lastname;
	private String address;
	private String zipcode;
	private String city;
	private LocalDate birthdayStart;
	private LocalDate birthdayEnd;

	private int pageSize;
	private int page;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public LocalDate getBirthdayStart() {
		return birthdayStart;
	}

	public void setBirthdayStart(LocalDate birthdayStart) {
		this.birthdayStart = birthdayStart;
	}

	public LocalDate getBirthdayEnd() {
		return birthdayEnd;
	}

	public void setBirthdayEnd(LocalDate birthdayEnd) {
		this.birthdayEnd = birthdayEnd;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}