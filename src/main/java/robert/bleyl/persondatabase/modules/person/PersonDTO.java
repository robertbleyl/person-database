package robert.bleyl.persondatabase.modules.person;

import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

public class PersonDTO {

	private long id;

	@NotBlank
	private String firstname;

	@NotBlank
	private String lastname;

	@NotBlank
	private String address;

	@NotBlank
	private String zipcode;

	@NotBlank
	private String city;

	@NotNull
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate birthday;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, birthday, city, firstname, id, lastname, zipcode);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonDTO other = (PersonDTO)obj;
		return Objects.equals(address, other.address) && Objects.equals(birthday, other.birthday) && Objects.equals(city, other.city) &&
				Objects.equals(firstname, other.firstname) && id == other.id && Objects.equals(lastname, other.lastname) &&
				Objects.equals(zipcode, other.zipcode);
	}

	@Override
	public String toString() {
		return "PersonDTO [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", address=" + address + ", zipcode=" + zipcode + ", city=" +
				city + ", birthday=" + birthday + "]";
	}
}