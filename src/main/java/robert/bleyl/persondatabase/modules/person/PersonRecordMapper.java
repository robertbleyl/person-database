package robert.bleyl.persondatabase.modules.person;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import robert.bleyl.persondatabase.modules.person.database.PersonRecord;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PersonRecordMapper {

	PersonDTO mapToDto(PersonRecord record);

	PersonRecord mapToRecord(PersonDTO dto);

	@Mapping(target = "id", ignore = true)
	void updateRecord(@MappingTarget PersonRecord record, PersonDTO dto);
}