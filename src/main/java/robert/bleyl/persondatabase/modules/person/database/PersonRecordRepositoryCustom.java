package robert.bleyl.persondatabase.modules.person.database;

import org.springframework.data.domain.Page;

import robert.bleyl.persondatabase.modules.person.PersonDTO;
import robert.bleyl.persondatabase.modules.person.PersonSearchRequest;

public interface PersonRecordRepositoryCustom {

	Page<PersonDTO> searchPersons(PersonSearchRequest request);
}