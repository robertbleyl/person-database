package robert.bleyl.persondatabase.modules.person;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

@RestController
public class PersonController {

	private static final Logger logger = LoggerFactory.getLogger(PersonController.class);

	private final PersonService personService;
	private final Validator validator;
	private final ObjectMapper objectMapper;

	@Autowired
	public PersonController(PersonService personService, Validator validator, ObjectMapper objectMapper) {
		this.personService = personService;
		this.validator = validator;
		this.objectMapper = objectMapper;
	}

	@GetMapping("/persons/{personId}")
	public ResponseEntity<Object> getPerson(@PathVariable("personId") long personId) {
		Optional<PersonDTO> opt = personService.getPerson(personId);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(opt.get());
	}

	@GetMapping(value = "/persons/{personId}/{field}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<Object> getPersonFieldValue(@PathVariable("personId") long personId, @PathVariable("field") String field) {
		Optional<PersonDTO> opt = personService.getPerson(personId);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		try {
			String json = objectMapper.writeValueAsString(opt.get());

			DocumentContext jsonContext = JsonPath.parse(json);
			String fieldValue = jsonContext.read("['" + field + "']");

			return ResponseEntity.ok(fieldValue);
		} catch (JsonProcessingException e) {
			logger.error("Error while trying to convert Person to JSON: ", e);
			return ResponseEntity.internalServerError().body("Unable to convert Person to JSON: " + e.getMessage());
		} catch (PathNotFoundException e) {
			return ResponseEntity.badRequest().body("Field " + field + " was not found!");
		}
	}

	@GetMapping("/persons/query")
	public PersonSearchResponseDTO searchPersons(
			@RequestParam(value = "firstname", required = false) String firstname,
			@RequestParam(value = "lastname", required = false) String lastname,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "zipcode", required = false) String zipcode,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "birthdayStart", required = false) LocalDate birthdayStart,
			@RequestParam(value = "birthdayEnd", required = false) LocalDate birthdayEnd,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			@RequestParam(value = "page", defaultValue = "0") int page) {

		PersonSearchRequest request = new PersonSearchRequest();
		request.setFirstname(firstname);
		request.setLastname(lastname);
		request.setAddress(address);
		request.setZipcode(zipcode);
		request.setCity(city);
		request.setBirthdayStart(birthdayStart);
		request.setBirthdayEnd(birthdayEnd);
		request.setPageSize(pageSize);
		request.setPage(page);

		return personService.searchPersons(request);
	}

	@PostMapping("/persons")
	public ResponseEntity<Object> addNewPerson(@Valid @RequestBody PersonDTO dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return ResponseEntity.badRequest().body(bindingResult.getAllErrors().toString());
		}

		long personId = personService.addPerson(dto);

		return ResponseEntity.ok(personId);
	}

	@PutMapping("/persons/{personId}")
	public ResponseEntity<Object> updatePerson(@PathVariable("personId") long personId, @Valid @RequestBody PersonDTO dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return ResponseEntity.badRequest().body(bindingResult.getAllErrors().toString());
		}

		boolean updated = personService.updatePerson(personId, dto);

		if (!updated) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/persons/{personId}")
	public ResponseEntity<Object> deletePerson(@PathVariable("personId") long personId) {
		boolean deleted = personService.deletePerson(personId);

		if (!deleted) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.noContent().build();
	}

	@PostMapping(value = "/persons/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Object> uploadCsv(@RequestParam("file") MultipartFile file) {
		if (file == null) {
			return ResponseEntity.badRequest().body("No file uploaded!");
		}

		try {
			InputStream inputStream = file.getInputStream();

			CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
			CsvMapper mapper = new CsvMapper();
			MappingIterator<PersonDTO> readValues = mapper.readerFor(PersonDTO.class).with(bootstrapSchema).readValues(inputStream);
			List<PersonDTO> allValues = readValues.readAll();

			for (PersonDTO personDTO : allValues) {
				Errors validationResult = new BeanPropertyBindingResult(personDTO, "person");
				validator.validate(personDTO, validationResult);

				if (validationResult.hasErrors()) {
					return ResponseEntity.badRequest().body(validationResult.getAllErrors().toString());
				}
			}

			personService.addPersons(allValues);

			return ResponseEntity.noContent().build();
		} catch (IOException e) {
			logger.error("Error while trying to parse uploaded CSV file: ", e);
			return ResponseEntity.badRequest().body("Unable to parse file: " + e.getMessage());
		}
	}
}