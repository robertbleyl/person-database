package robert.bleyl.persondatabase.modules.person.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import robert.bleyl.persondatabase.modules.person.PersonDTO;
import robert.bleyl.persondatabase.modules.person.PersonSearchRequest;

public class PersonRecordRepositoryImpl implements PersonRecordRepositoryCustom {

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public PersonRecordRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Page<PersonDTO> searchPersons(PersonSearchRequest request) {
		int page = request.getPage();
		int pageSize = request.getPageSize();
		int offset = page * pageSize;

		List<String> wheres = new ArrayList<>();
		Map<String, Object> queryParams = new HashMap<>();

		if (StringUtils.isNoneBlank(request.getFirstname())) {
			wheres.add("firstname ILIKE :firstname");
			queryParams.put("firstname", "%" + request.getFirstname() + "%");
		}

		addStringFieldToWheres(wheres, queryParams, "firstname", request.getFirstname());
		addStringFieldToWheres(wheres, queryParams, "lastname", request.getLastname());
		addStringFieldToWheres(wheres, queryParams, "address", request.getAddress());
		addStringFieldToWheres(wheres, queryParams, "zipcode", request.getZipcode());
		addStringFieldToWheres(wheres, queryParams, "city", request.getCity());

		if (request.getBirthdayStart() != null) {
			wheres.add("birthday >= :birthdayStart");
			queryParams.put("birthdayStart", request.getBirthdayStart());
		}

		if (request.getBirthdayEnd() != null) {
			wheres.add("birthday <= :birthdayEnd");
			queryParams.put("birthdayEnd", request.getBirthdayEnd());
		}

		String where = "";

		if (!wheres.isEmpty()) {
			where = " WHERE " + String.join(" AND ", wheres);
		}

		String contentSql = """
				SELECT
					id,
					firstname,
					lastname
					address,
					zipcode,
					city,
					birthday
				FROM
					person
				""" + where +
				" LIMIT " + pageSize +
				" OFFSET " + offset;

		RowMapper<PersonDTO> rowMapper = new BeanPropertyRowMapper<>(PersonDTO.class);
		List<PersonDTO> content = jdbcTemplate.query(contentSql, queryParams, rowMapper);

		String countSql = "SELECT COUNT(id) FROM person" + where;
		long totalElements = jdbcTemplate.queryForObject(countSql, queryParams, Long.class);

		return new PageImpl<>(content, PageRequest.of(page, pageSize), totalElements);
	}

	private void addStringFieldToWheres(List<String> wheres, Map<String, Object> queryParams, String fieldName, String value) {
		if (StringUtils.isNoneBlank(value)) {
			wheres.add(fieldName + " ILIKE :" + fieldName);
			queryParams.put(fieldName, "%" + value + "%");
		}
	}
}