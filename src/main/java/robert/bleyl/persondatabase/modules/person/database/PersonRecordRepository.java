package robert.bleyl.persondatabase.modules.person.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRecordRepository extends JpaRepository<PersonRecord, Long>, PersonRecordRepositoryCustom {

}