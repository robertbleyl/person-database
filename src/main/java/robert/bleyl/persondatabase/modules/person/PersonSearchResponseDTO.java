package robert.bleyl.persondatabase.modules.person;

import java.util.List;

public class PersonSearchResponseDTO {

	private long totalElements;
	private List<PersonDTO> results;

	public PersonSearchResponseDTO() {

	}

	public PersonSearchResponseDTO(long totalElements, List<PersonDTO> results) {
		this.totalElements = totalElements;
		this.results = results;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public List<PersonDTO> getResults() {
		return results;
	}

	public void setResults(List<PersonDTO> results) {
		this.results = results;
	}
}