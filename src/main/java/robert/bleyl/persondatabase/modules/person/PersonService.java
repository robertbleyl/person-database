package robert.bleyl.persondatabase.modules.person;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import robert.bleyl.persondatabase.modules.person.database.PersonRecord;
import robert.bleyl.persondatabase.modules.person.database.PersonRecordRepository;

@Service
public class PersonService {

	private final PersonRecordRepository personRecordRepository;
	private final PersonRecordMapper personRecordMapper;

	@Autowired
	public PersonService(PersonRecordRepository personRecordRepository, PersonRecordMapper personRecordMapper) {
		this.personRecordRepository = personRecordRepository;
		this.personRecordMapper = personRecordMapper;
	}

	public Optional<PersonDTO> getPerson(long personId) {
		Optional<PersonRecord> opt = personRecordRepository.findById(personId);
		return opt.map(personRecordMapper::mapToDto);
	}

	public PersonSearchResponseDTO searchPersons(PersonSearchRequest request) {
		Page<PersonDTO> result = personRecordRepository.searchPersons(request);
		return new PersonSearchResponseDTO(result.getTotalElements(), result.getContent());
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long addPerson(PersonDTO person) {
		PersonRecord personRecord = personRecordMapper.mapToRecord(person);
		personRecord = personRecordRepository.save(personRecord);
		return personRecord.getId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean updatePerson(long personId, PersonDTO dto) {
		Optional<PersonRecord> opt = personRecordRepository.findById(personId);

		if (opt.isEmpty()) {
			return false;
		}

		PersonRecord record = opt.get();
		personRecordMapper.updateRecord(record, dto);

		personRecordRepository.save(record);

		return true;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean deletePerson(long personId) {
		Optional<PersonRecord> opt = personRecordRepository.findById(personId);

		if (opt.isEmpty()) {
			return false;
		}

		personRecordRepository.delete(opt.get());

		return true;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addPersons(List<PersonDTO> personDtos) {
		for (PersonDTO dto : personDtos) {
			PersonRecord record = personRecordMapper.mapToRecord(dto);
			personRecordRepository.save(record);
		}
	}
}