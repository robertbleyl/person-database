package robert.bleyl.persondatabase.modules.person;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import io.restassured.RestAssured;
import robert.bleyl.persondatabase.modules.person.database.PersonRecordRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Testcontainers
class AbstractIntegrationTest {

	@Container
	PostgreSQLContainer<?> psql = new PostgreSQLContainer<>(PostgreSQLContainer.IMAGE + ":" + PostgreSQLContainer.DEFAULT_TAG)
			.withDatabaseName("testdatabasename")
			.withUsername("testuser")
			.withPassword("testpw");

	@Autowired
	PersonRecordRepository personRecordRepository;

	@BeforeClass
	static void setupConnection() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
	}

	@AfterEach
	void cleanup() {
		personRecordRepository.deleteAll();
	}

	PersonDTO createTestPersonDto() {
		PersonDTO requestDto = new PersonDTO();
		requestDto.setFirstname("testfirstname");
		requestDto.setLastname("testlastname");
		requestDto.setAddress("testaddress");
		requestDto.setZipcode("testzipcode");
		requestDto.setCity("testcity");
		requestDto.setBirthday(LocalDate.parse("1988-01-07"));
		return requestDto;
	}
}
