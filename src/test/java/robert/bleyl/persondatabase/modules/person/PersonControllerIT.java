package robert.bleyl.persondatabase.modules.person;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;

class PersonControllerIT extends AbstractIntegrationTest {

	RequestSpecBuilder requestSpecBuilder;

	@BeforeEach
	void setup() {
		requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader());
	}

	@Test
	void shouldReturnBadRequestBecauseAllFieldsBlank() {
		given().log().all().spec(requestSpecBuilder.build())
				.body(new PersonDTO())
				.when().post("/persons/")
				.then().statusCode(400);
	}

	@Test
	void shouldAddAndRetrieveAndUpdateAndDelete() {
		PersonDTO requestDto = createTestPersonDto();

		Long personId = given().log().all().spec(requestSpecBuilder.build())
				.body(requestDto)
				.when().post("/persons/")
				.then().statusCode(200).extract().as(Long.class);

		assertTrue(personId > 0L);

		PersonDTO responseDto = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/" + personId)
				.then().statusCode(200).extract().as(PersonDTO.class);

		assertEquals(personId, responseDto.getId());
		assertEquals(responseDto.getFirstname(), requestDto.getFirstname());
		assertEquals(responseDto.getLastname(), requestDto.getLastname());
		assertEquals(responseDto.getAddress(), requestDto.getAddress());
		assertEquals(responseDto.getZipcode(), requestDto.getZipcode());
		assertEquals(responseDto.getCity(), requestDto.getCity());
		assertEquals(responseDto.getBirthday(), requestDto.getBirthday());

		requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.TEXT).addHeader("Accept", ContentType.TEXT.getAcceptHeader());

		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/" + personId + "/firstname")
				.then().statusCode(200).body(equalTo(requestDto.getFirstname()));

		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/" + personId + "/birthday")
				.then().statusCode(200).body(equalTo(requestDto.getBirthday().toString()));

		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/" + personId + "/someinvalidfield")
				.then().statusCode(400).body(equalTo("Field someinvalidfield was not found!"));

		setup();

		requestDto.setAddress("newaddress");

		given().log().all().spec(requestSpecBuilder.build())
				.body(requestDto)
				.when().put("/persons/" + personId)
				.then().statusCode(204);

		responseDto = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/" + personId)
				.then().statusCode(200).extract().as(PersonDTO.class);

		assertEquals(personId, responseDto.getId());
		assertEquals(responseDto.getFirstname(), requestDto.getFirstname());
		assertEquals(responseDto.getLastname(), requestDto.getLastname());
		assertEquals(responseDto.getAddress(), requestDto.getAddress());
		assertEquals(responseDto.getZipcode(), requestDto.getZipcode());
		assertEquals(responseDto.getCity(), requestDto.getCity());
		assertEquals(responseDto.getBirthday(), requestDto.getBirthday());

		given().log().all().spec(requestSpecBuilder.build())
				.body(new PersonDTO())
				.when().delete("/persons/" + personId)

				.then().statusCode(204);
		given().log().all().spec(requestSpecBuilder.build())
				.body(new PersonDTO())
				.when().get("/persons/" + personId)
				.then().statusCode(404);
	}

	@Test
	void shouldReturnNotFoundWhenGetBecausePersonNotPresent() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/1234")
				.then().statusCode(404);
	}

	@Test
	void shouldReturnNotFoundWhenPutBecausePersonNotPresent() {
		given().log().all().spec(requestSpecBuilder.build())
				.body(createTestPersonDto())
				.when().put("/persons/1234")
				.then().statusCode(404);
	}

	@Test
	void shouldReturnBadRequestWhenPutBecauseAllFieldsBlank() {
		given().log().all().spec(requestSpecBuilder.build())
				.body(new PersonDTO())
				.when().put("/persons/1234")
				.then().statusCode(400);
	}

	@Test
	void shouldReturnNotFoundWhenDeleteBecausePersonNotPresent() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().delete("/persons/1234")
				.then().statusCode(404);
	}

	@Test
	void shouldReturnNotFoundWhenLookingForFieldValueBecausePersonNotPresent() {
		requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.TEXT).addHeader("Accept", ContentType.TEXT.getAcceptHeader());

		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/1234/birthday")
				.then().statusCode(404);
	}
}