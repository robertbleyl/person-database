package robert.bleyl.persondatabase.modules.person;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;

class PersonCSVUploadIT extends AbstractIntegrationTest {

	RequestSpecBuilder requestSpecBuilder;

	@BeforeEach
	void setup() {
		requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.MULTIPART).addHeader("Accept", ContentType.MULTIPART.getAcceptHeader());
	}

	@Test
	void shouldReturnBadRequestBecauseNoFile() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().post("/persons/csv")
				.then().statusCode(500);
	}

	@Test
	void shouldReturnBadRequestBecauseUnableToParseFile() {
		given().log().all().spec(requestSpecBuilder.build())
				.multiPart("file", new File("src/test/resources/invalid.csv"))
				.when().post("/persons/csv")
				.then().statusCode(400).body(containsString("Unable to parse file: Unrecognized field \"header1\""));
	}

	@Test
	void shouldAddNoDataBecauseFileHasOnlyHeader() {
		given().log().all().spec(requestSpecBuilder.build())
				.multiPart("file", new File("src/test/resources/only_header.csv"))
				.when().post("/persons/csv")
				.then().statusCode(204);

		assertEquals(0L, personRecordRepository.count());
	}

	@Test
	void shouldAddNewPersonsViaCSV() {
		given().log().all().spec(requestSpecBuilder.build())
				.multiPart("file", new File("src/test/resources/add_and_update.csv"))
				.when().post("/persons/csv")
				.then().statusCode(204);

		assertEquals(2L, personRecordRepository.count());
	}
}