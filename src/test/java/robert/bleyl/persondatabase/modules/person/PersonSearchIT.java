package robert.bleyl.persondatabase.modules.person;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import robert.bleyl.persondatabase.modules.person.database.PersonRecord;

class PersonSearchIT extends AbstractIntegrationTest {

	RequestSpecBuilder requestSpecBuilder;

	@BeforeEach
	void setup() {
		PersonRecord person1 = new PersonRecord();
		person1.setFirstname("testfirstname1");
		person1.setLastname("testlastname1");
		person1.setAddress("testaddress1");
		person1.setZipcode("testzipcode1");
		person1.setCity("testcity1");
		person1.setBirthday(LocalDate.parse("1990-11-27"));

		PersonRecord person2 = new PersonRecord();
		person2.setFirstname("testfirstname2");
		person2.setLastname("testlastname2");
		person2.setAddress("testaddress2");
		person2.setZipcode("testzipcode2");
		person2.setCity("testcity2");
		person2.setBirthday(LocalDate.parse("1988-01-07"));

		personRecordRepository.saveAll(List.of(person1, person2));

		requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader());
	}

	@Test
	void shouldReturnAllPersonsBecauseNoParameters() {
		PersonSearchResponseDTO response = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/query")
				.then().statusCode(200).extract().as(PersonSearchResponseDTO.class);

		assertEquals(response.getTotalElements(), 2L);
		List<PersonDTO> results = response.getResults();
		assertEquals(2, results.size());
	}

	@Test
	void shouldReturnAllPersonsBecauseFirstnameMatchesMultiple() {
		PersonSearchResponseDTO response = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/query?firstname=testfirstname")
				.then().statusCode(200).extract().as(PersonSearchResponseDTO.class);

		assertEquals(response.getTotalElements(), 2L);
		List<PersonDTO> results = response.getResults();
		assertEquals(2, results.size());
	}

	@Test
	void shouldReturnOnePersonBecauseFirstnameMatchesExactlyOne() {
		PersonSearchResponseDTO response = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/query?firstname=testfirstname1")
				.then().statusCode(200).extract().as(PersonSearchResponseDTO.class);

		assertEquals(response.getTotalElements(), 1L);
		List<PersonDTO> results = response.getResults();
		assertEquals(1, results.size());
	}

	@Test
	void shouldReturnOnePersonBecausePageSize() {
		PersonSearchResponseDTO response = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/query?pageSize=1")
				.then().statusCode(200).extract().as(PersonSearchResponseDTO.class);

		assertEquals(response.getTotalElements(), 2L);
		List<PersonDTO> results = response.getResults();
		assertEquals(1, results.size());
	}

	@Test
	void shouldReturnNoPersonsBecausePageTooHigh() {
		PersonSearchResponseDTO response = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/persons/query?page=1")
				.then().statusCode(200).extract().as(PersonSearchResponseDTO.class);

		assertEquals(response.getTotalElements(), 2L);
		List<PersonDTO> results = response.getResults();
		assertEquals(0, results.size());
	}
}